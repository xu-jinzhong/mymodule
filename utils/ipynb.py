import os
import json
import io
import random
from nbformat import read
from IPython.display import HTML
from IPython.display import clear_output as co
from IPython.core.getipython import get_ipython

def run_ipynb(ipynb, clear_output=False):
    """
    获取其他IPYNB中的可运行代码，并运行。可设置是否清空运行结果
    """
    ipynb = os.path.abspath(ipynb) # 另一个ipynb文件
    ip = get_ipython()
    with io.open(ipynb, 'r', encoding='utf-8') as f: # 读ipynb文件内容
        nb = read(f, 2)
        for cell in nb.worksheets[0].cells:
            if cell.cell_type == 'code': # 对ipynb文件中的每个cell运行代码
                ip.run_cell(cell.input)
    if clear_output: 
        # 把运行结果清除
        co(wait=False)
        
def get_codes(ipynb):
    """
    从其他ipynb中获取代码字符串，返回结果是列表，元素是单元格中的代码字符串
    """
    ipynb = os.path.abspath(ipynb) # 另一个ipynb文件
    with io.open(ipynb, 'r', encoding='utf-8') as f: # 读ipynb文件内容
        nb = read(f, 2)
    ip = get_ipython()
    codes = []
    for cell in nb.worksheets[0].cells:
        if (cell.cell_type == 'code') and (cell.input != ''): 
            # 对ipynb文件中的每个cell代码收集起来
            codes.append(cell.input)

    return codes
    
def new_cells(codes):
    """
    根据代码字符串，创建新的单元格
    注意：目前在 notebook 中可创建多个单元格；但在 lab 中只能创建一个
    """
    ip = get_ipython()
    payload = dict(
        source='set_next_input',
        text=codes,
        replace=False,
    )
    ip.payload_manager.write_payload(payload, single=False)

def hide_toggle(for_next=False):
  this_cell = """$('div.cell.code_cell.rendered.selected')"""
  next_cell = this_cell + '.next()'

  toggle_text = '打开/隐藏'  # text shown on toggle link
  target_cell = this_cell  # target cell to control with toggle
  js_hide_current = ''  # bit of JS to permanently hide code in current cell (only when toggling next cell)

  if for_next:
    target_cell = next_cell
    toggle_text += ' next cell'
    js_hide_current = this_cell + '.find("div.input").hide();'

  js_f_name = 'code_toggle_{}'.format(str(random.randint(1,2**64)))

  html = """
      <script>
          function {f_name}() {{
              {cell_selector}.find('div.input').toggle();
          }}

          {js_hide_current}
      </script>

      <a href="javascript:{f_name}()">{toggle_text}</a>
      """.format(
          f_name=js_f_name,
          cell_selector=target_cell,
          js_hide_current=js_hide_current, 
          toggle_text=toggle_text
      )

  return HTML(html)

def hide_cells():
  return HTML('''<script>
        code_show=true; 
        function code_toggle() {
          if (code_show){
          $('.cm-comment:contains(@hidden)').closest('div.input').hide();
          } else {
          $('.cm-comment:contains(@hidden)').closest('div.input').show();
          }
          code_show = !code_show
        } 
        $( document ).ready(code_toggle);
        </script>
        默认关闭单元格，想打开请点击 <a href="javascript:code_toggle()">here</a>.''')