"""
装饰器用来计算函数运行时间
"""
import time


def display_time(text):
    """给装饰器增加参数"""
    def decorator(func):
        """装饰器函数，参数为函数"""
        def wrapper(*args, **kwargs):
            """计算函数运行时间"""
            start_time = time.time()
            result = func(*args, **kwargs)
            end_time = time.time()
            print(f'{text} time consumed: {end_time - start_time} seconds')
            return result

        return wrapper

    return decorator
