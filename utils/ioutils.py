
"""
文件操作
1. 下载文件
"""
import glob
import os
import zipfile

import tqdm


def download(url, filename=None, proxy="127.0.0.1:8118"):
    r"""Download file from the internet using aria2c.

    Args:
        url (string): URL of the internet file.
        filename (string): Path to store the downloaded file.
        proxy (string): http proxy
    """
    if isinstance(url, str):  # 对某一个链接
        if filename is None:
            cmd = f"aria2c -c --http-proxy={proxy} --seed-time=0 {url}"
        else:
            file_path, file_name = os.path.split(filename)
            cmd = f"aria2c -c --http-proxy={proxy} --seed-time=0 -d {file_path} -o {file_name} {url}"
    else:  # 对于多个链接写入到 txt 文件，以换行符分割
        if os.path.isdir(filename):
            file_path = filename
        if os.path.isfile(filename):
            file_path = os.path.dirname(filename)
            cmd = (
                f"aria2c -c --http-proxy={proxy} --seed-time=0 -d {file_path} -i {url}"
            )
    return os.system(cmd)


def extract(zip_path, extract_dir=None):
    """
    解压缩zip文件或文件夹

    Args:
        zip_path: zip 文件或包含多个zip文件的文件夹
        extract_dir: 存放解压缩后的文件的路径
    """
    if extract_dir is not None:  # 如果存放解压缩文件的路径不存在则创建
        if not os.path.exists(extract_dir):
            os.makedirs(extract_dir)
    if os.path.isfile(zip_path):  # 对于单个zip文件
        file = zip_path
        if extract_dir is None:
            extract_dir = os.path.basename(file)
        if zipfile.is_zipfile(file):
            with zipfile.ZipFile(file, "r") as f:
                f.extractall(extract_dir)
    if os.path.isdir(zip_path):  # 对于包含多个zip文件的问价夹
        if extract_dir is None:
            extract_dir = zip_path
        zip_list = glob.glob(zip_path + "*.zip")
        for file in tqdm.tqdm(zip_list):
            if zipfile.is_zipfile(file):
                with zipfile.ZipFile(file, "r") as f:
                    f.extractall(extract_dir)
