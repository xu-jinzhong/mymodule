# 获取变量名


def get_variable_name(variable_name):
    variable_names = [k for k, v in globals().items() if v == variable_name]
    return variable_names[0]
