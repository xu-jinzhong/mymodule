# 文件、文件夹处理相关模块
import os


def walk(dirs):
    """
    给定一个文件夹，遍历该文件夹先的所有文件路径，以列表形式返回
    """
    files_all = []
    for root, d, files in os.walk(dirs):
        for file in files:
            file_path = os.path.join(root, file)
            files_all.append(file_path)
    return files_all


def scan(dirs):
    """
    给定一个文件夹，遍历该文件夹先的所有文件路径，以列表形式返回
    """
    files_all = []
    for item in os.scandir(dirs):
        if item.is_file():
            files_all.append(item.path)
        if item.is_dir():  # 对于目录，递归检索图片
            files_all += scan(dirs=item.path)
    return files_all
