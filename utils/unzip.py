#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : Jinzhong Xu
# @Contact : jinzhongxu@csu.ac.cn
# @Time    : 2021/7/5 13:34
# @File    : unzip.py
# @Software: PyCharm

import glob
import zipfile
from tqdm import tqdm
import os


def unzip(zip_path, out_path=None):
    """
    批量解压缩.zip文件到指定的文件夹
    :param zip_path: .zip 文件夹路径
    :param out_path: 解压缩输出的文件夹，当为空时，设置成压缩文件所在文件夹路径
    :return: 0 或 done!
    """
    # 通过abspath后得到的路径最后没有‘/’，如‘/home/jinzhongxu’
    zip_path = os.path.abspath(zip_path)
    assert os.path.isdir(zip_path), print("请输入文件夹路径zip_path")
    # 如果输出文件夹为空，这指向.zip文件夹
    if not out_path:
        out_path = zip_path
    assert os.path.isdir(out_path), print("请输入文件夹路径out_path")
    # 罗列出所有.zip压缩文件
    zip_list = glob.glob(zip_path + '/*.zip')
    if len(zip_list) == 0:
        print(f"在路径{zip_path}下没有发现压缩文件！！！")
        return 0
    # 如果有压缩文件，则解压缩到out_path文件夹
    for file in tqdm(zip_list):
        if zipfile.is_zipfile(file):
            with zipfile.ZipFile(file, 'r') as f:
                f.extractall(out_path)
    return "done!"


if __name__ == '__main__':
    path = '.'
    unzip(path)
