"""
工作相关辅助分析函数
"""

def time_analysis(eng=3, sci=3, stu=1, o=1):
    """
    简单的统计函数，计算周报中各类别的时间占比
    输入参数的单位为天
    """
    total = eng + sci + stu + o
    eng_ratio = eng / total
    sci_ratio = sci / total
    stu_ratio = stu / total
    o_ratio = o / total
    print('总时间是 {:04.2f} 天'.format(total))
    print('工程 {} 天，占比 {:04.2f}%'.format(eng, eng_ratio * 100))
    print('科研 {} 天，占比 {:04.2f}%'.format(sci, sci_ratio * 100))
    print('教辅 {} 天，占比 {:04.2f}%'.format(stu, stu_ratio * 100))
    print('其他 {} 天，占比 {:04.2f}%'.format(o, o_ratio * 100))
    
    
def honey_salary(performance, fixed_mark=70000, subsidy=700, social_security=566):
    """
    帮助计算工资
    performance: 业绩
    fixed_mark: 业绩比较值，固定 7W，当业绩大于等于 fixed_mark 时，底薪为 8K，提成为业绩的 5%；
                否则，底薪为 7K，提成为业绩 4%
    subsidy: 补助，固定 7B
    social_security: 社保扣除额
    """
    # 基本工资，即底薪。当业绩大于等于 7W 时，底薪为 8K；当业绩小于 7W 时，底薪为 7K
    basic_salary = 8000 if performance >= fixed_mark else 7000
    # 提成。当业绩大于等于 7W 时，提成为业绩的 5%；当业绩小于 7W 时，提成为业绩的 4%
    commission = performance * 0.05 if performance >= fixed_mark else performance * 0.04
    # 到手工资。基本工资 + 提成 + 补贴 - 社保扣除额
    salary = basic_salary + commission + subsidy - social_security
    return salary
