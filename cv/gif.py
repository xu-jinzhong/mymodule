#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author  : Jinzhong Xu
# @Contact : jinzhongxu@csu.ac.cn
# @Time    : 2021/8/6 15:21
# @File    : git.py
# @Software: JupyterHub


from PIL import Image
import glob
import os
import imageio
import tqdm


def img2gif_imageio(imgPath, imgExtension="jpg"):
    """
    把图片合并为GIF
    imgPath: 输入图片路径
    imgExtension: 输入图片扩展格式
    """
    imgPath = os.path.abspath(imgPath)
    imgs = sorted(glob.glob(imgPath + "/*." + imgExtension))
    print(f"总共检索到{len(imgs)}幅图像")

    assert len(imgs) > 0, "图片数必须大于0"

    gifName = "a.gif"
    with imageio.get_writer(imgPath + "/" + gifName, mode="I") as writer:
        for imgFile in tqdm.tqdm(imgs):
            img = imageio.imread(imgFile)
            writer.append_data(img)

    print(f"合并完成，请在{imgPath}下查看视频文件{gifName}!")


def img2gif_pil(imgPath, imgExtension="jpg"):
    """
    把图片合并为GIF
    imgPath: 输入图片路径
    imgExtension: 输入图片扩展格式
    """
    imgPath = os.path.abspath(imgPath)
    imgs = sorted(glob.glob(imgPath + "/*." + imgExtension))
    print(f"总共检索到{len(imgs)}幅图像")

    assert len(imgs) > 0, "图片数必须大于0"

    gifName = "a.gif"
    gifPath = imgPath + "/" + gifName

    img, *images = [Image.open(f) for f in imgs]
    img.save(
        fp=gifPath,
        format="GIF",
        append_images=images,
        save_all=True,
        duration=200,
        loop=0,
    )
    print(f"合并完成，请在{imgPath}下查看视频文件{gifName}!")