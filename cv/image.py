#!/usr/bin/env python
# @Author  : Jinzhong Xu
# @Contact : jinzhongxu@csu.ac.cn
# @Time    : 2021/7/23 11:31
# @File    : image.py
# @Software: JupyterHub

    
import numpy as np
import os
from PIL import Image
import cv2
import urllib
import matplotlib.pyplot as plt
import pyheif


def url_to_image(url):
	# download the image, convert it to a NumPy array, and then read
	# it into OpenCV format
	resp = urllib.request.urlopen(url)
	image = np.asarray(bytearray(resp.read()), dtype="uint8")
	image = cv2.imdecode(image, cv2.IMREAD_COLOR)
	return image


def im_show(img, cmap=None):
    # plt.imshow(cv2.merge(cv2.split(img)[::-1]))
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB), cmap=cmap)
    plt.show()


def convert_img_pil(imgpath, extension="jpg"):
    """
    使用pil转换图像格式，如man.jpg -> man.png
    imgpath: 图像路径
    extension: 新扩展名
    结果图像保存到原文件目录下
    """
    img = Image.open(imgpath)
    img = img.convert("RGB")
    imgfile = imgpath.split(".")
    img.save(imgfile[0] + "." + extension)
    
    
def convert_img_cv2(imgpath, extension="jpg"):
    """
    使用opencv转换图像格式，如man.jpg -> man.png
    imgpath: 图像路径
    extension: 新扩展名
    结果图像保存到原文件路径下
    """
    img = cv2.imread(imgpath)
    imgfile = imgpath.split(".")
    if extension == 'jpg':
        cv2.imwrite(imgfile[0] + "." + extension, img, [cv2.IMWRITE_JPEG_QUALITY, 80])
    elif extension == 'png':
        cv2.imwrite(imgfile[0] + "." + extension, img, [cv2.IMWRITE_PNG_COMPRESSION, 8])
        

def convert_heic2png(imgpath):
    """
    把iPhone拍摄的HEIC格式的图片转化为PNG格式
    imgpath: iPhone拍摄的HEIC格式的图片
    """
    new_name = imgpath.replace("HEIC", "png").replace("heic", "png")
    heif_file = pyheif.read(imgpath)
    data = Image.frombytes(
        heif_file.mode,
        heif_file.size,
        heif_file.data,
        "raw",
        heif_file.mode,
        heif_file.stride,
    )
    data.save(new_name, "png")
    
def edge_detection(imgpath, threshold1=30, threshold2=100):
    """
    使用opencv中的canny进行图像编译检测
    imgpath: 图像路径
    threshold1: canny检测算子低阈值
    threshold2: canny检测算子高阈值
    结果图像保存到原文件路径下
    
    cv2.imread(imgpath, flags)
    flags 可以取如下值：
    cv2.IMREAD_COLOR : 默认使用该种标识。加载一张彩色图片，忽视它的透明度， 不包括它的Alpha通道。
    cv2.IMREAD_GRAYSCALE : 加载一张灰度图。
    cv2.IMREAD_UNCHANGED : 加载图像，包括它的Alpha通道。
    可以简单的使用1，0，-1分别代替。
    """
    img = cv2.imread(imgpath)
    gray = cv2.cvtColor(img, cv2.COLOR_RGBA2GRAY)
    imgG = cv2.GaussianBlur(gray, (3, 3), 0)
    dst = cv2.Canny(imgG, threshold1, threshold2)
    imgfile = imgpath.split(".")
    cv2.imwrite(imgfile[0] + "_edge." + imgfile[1], dst)
    

def sketch(imgPath):
    """
    对输入图像计算得到素描图
    imgPath: 图像路径
    """
    imgPath = os.path.abspath(imgPath)
    imgDir, imgName = os.path.split(imgPath)
    sketchImgName = 'sketch-' + imgName
    sketchImgPath = os.path.join(imgDir, sketchImgName)
    
    a = np.asarray(Image.open(imgPath).convert("L")).astype("float")

    depth = 10.0  # (0-100)
    grad = np.gradient(a)  # 取图像灰度的梯度值
    grad_x, grad_y = grad  # 分别取横纵图像梯度值
    grad_x = grad_x * depth / 100.0
    grad_y = grad_y * depth / 100.0
    A = np.sqrt(grad_x ** 2 + grad_y ** 2 + 1.0)
    uni_x = grad_x / A
    uni_y = grad_y / A
    uni_z = 1.0 / A

    vec_el = np.pi / 2.2  # 光源的俯视角度，弧度值
    vec_az = np.pi / 4.0  # 光源的方位角度，弧度值
    dx = np.cos(vec_el) * np.cos(vec_az)  # 光源对x 轴的影响
    dy = np.cos(vec_el) * np.sin(vec_az)  # 光源对y 轴的影响
    dz = np.sin(vec_el)  # 光源对z 轴的影响

    b = 255 * (dx * uni_x + dy * uni_y + dz * uni_z)  # 光源归一化
    b = b.clip(0, 255)

    im = Image.fromarray(b.astype("uint8"))  # 重构图像
    im.save(sketchImgPath)
    return 1
