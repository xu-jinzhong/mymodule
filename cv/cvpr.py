"""从CVPR下载PAPER"""
import os
import re
import requests
from . import PROJECT_PATH
import sys
sys.path.insert(0, PROJECT_PATH)
from utils.ioutils import download


def get_day_url_cvpr(year=2021):
    """
    按照年份获取CVPR主页中开会当天的文章网页地址
    year: (int) 年份
    """
    base_url = f"https://openaccess.thecvf.com/CVPR{year}"
    day_li = []
    r = requests.get(url=base_url)
    text_li = r.text.split("\n")
    for t in text_li:
        if ("href" in t) and ("day" in t) and ("all" not in t):
            day = re.compile(r"\"(.*)\"").findall(t)
            if day == []:
                continue
            else:
                day = day[0]
            day_url = "https://openaccess.thecvf.com/" + day
            day_li.append(day_url)
    return day_li


def download_paper_cvpr(year=2021, dest_dir=".", keywords=("Tracking",)):
    """
    按照年份从CVPR官网下载包含关键字的论文
    year: (int) 年份
    dest_dir: (str) 下载的文章保存文件夹地址
    keywords: (list) 文章标题中出现的关键词
    """
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    os.chdir(dest_dir)
    paper_count = 0
    for url in get_day_url_cvpr(year=year):
        response = requests.get(url=url)
        text_list = response.text.split("\n")
        for t in text_list:
            flag = True
            for k in keywords:
                if k not in t:
                    flag = False
            if ("href" in t) and ("pdf" in t) and flag:
                paper = re.compile(r"\"(.*?)\"").findall(t)  # 列表
                if paper == []:
                    continue
                else:
                    paper = paper[0]  # 此时 paper 为字符串
                if paper[0] == "/":
                    paper = paper[1:]
                url_paper = "https://openaccess.thecvf.com/" + paper
                paper_count += 1
                print(f"正在下载第 {paper_count} 篇")
                print(url_paper)
                download(url=url_paper)
    if paper_count == 0:
        print(f"未在CVPR官网（{year}）检索到包含关键字：{', '.join(keywords)} 的文章！")
    else:
        print("下载完成！")
        print(f"在CVPR官网（{year}）共下载 {paper_count} 篇包含关键字：{', '.join(keywords)} 的文章！")
