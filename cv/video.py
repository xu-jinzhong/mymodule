#!/usr/bin/env python
# @Author  : Jinzhong Xu
# @Contact : jinzhongxu@csu.ac.cn
# @Time    : 2021/8/5 18:53
# @File    : video.py
# @Software: JupyterHub


import glob
import os

import cv2
import pafy
import tqdm


def url_to_video(url, preftype="mp4"):
    """
    通过包pafy把视频地址转化为".mp4"的形式，输入给cv2.VideoCapture
    返回视频对象
    """
    vPafy = pafy.new(url)
    play = vPafy.getbest(preftype=preftype)
    vc = cv2.VideoCapture(play.url)
    return vc


def video2img(videoPath, startFrame=1, endFrame=-1):
    """
    从视频中提取每帧图像，图像格式为JPG
    videoPath: 视频路径
    startFrame: 起始帧，整数，1表示第一帧
    endFrame: 结束帧，整数，-1表示最后一帧
    """
    assert isinstance(startFrame, int), "起始帧应为整数"
    assert isinstance(endFrame, int), "结束帧应为整数"

    if startFrame <= 1:
        startFrame = 1
    if endFrame <= -1:
        endFrame = -1
    assert endFrame != 0, "结束帧应取-1 或者 正整数"

    if endFrame != -1:
        assert endFrame >= startFrame, "结束帧应大于起始帧"

    cap = cv2.VideoCapture(videoPath)  # 加载视频
    isOpened = cap.isOpened  # 是否加载成功

    videoPath = os.path.abspath(videoPath)
    video_dir = os.path.dirname(videoPath)
    video = os.path.basename(videoPath)
    video_name_ext = video.split(".")
    print(f"视频格式: {video_name_ext[1].upper()}")
    
    frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))  # 总帧数
    fps = cap.get(cv2.CAP_PROP_FPS)  # 帧率
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))  # 每帧图片宽度
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))  # 每帧图片高度
    frames_len = len(str(frames))
    print("视频总帧数: ", frames)
    print("视频帧率FPS: ", fps)
    print("视频图像宽W: ", width)
    print("视频图像高H: ", height)

    i = 0  # save pictures number
    while isOpened:
        if i == endFrame:
            break
        elif i < startFrame - 1:
            i += 1
            continue
        else:
            i += 1
            (flag, frame) = cap.read()  # flag: read success Yes; frame:one picture
            fileName = video_name_ext[0] + '_' + str(i).zfill(frames_len) + ".jpg"
            if flag:
                cv2.imwrite(
                    os.path.join(video_dir, fileName),
                    frame,
                    [cv2.IMWRITE_JPEG_QUALITY, 100],
                )
            else:
                break
    print(f"提取完成，请在{video_dir}下查看图像！")


def img2video(
    imgPath,
    videoPath=None,
    videoName="a",
    imgExtension="jpg",
    videoExtension="mp4",
    fps=30,
):
    """
    把图片合并为视频，视频格式为avi或者mp4
    imgPath: 输入图片路径
    videoPath: 输出视频路径，取值 None 时，表示输出到图片路径
    videoName: 输出视频名
    imgExtension: 输入图片扩展格式
    videoExtension: 输出视频扩展格式
    fps: 输出视频帧率
    """
    assert videoExtension.upper() in ["mp4".upper(), "avi".upper()], "输出视频必须是mp4或者avi"
    assert isinstance(fps, int), "输出视频帧率必须为整数"
    fps = abs(fps)
    if not os.path.exists(imgPath):
        print("图片路径不存在")
        return
    imgPath = os.path.abspath(imgPath)
    imgs = sorted(glob.glob(imgPath + "/*." + imgExtension))
    print(f"总共检索到{len(imgs)}幅图像")

    assert len(imgs) > 0, "图片数必须大于0"

    img0 = cv2.imread(imgs[0])  # 读取首幅图像，获取基本信息
    imgInfo = img0.shape  # 高、宽、通道
    frameSize = imgInfo[1], imgInfo[0]  # 宽、高
    print(f"图像宽：{imgInfo[1]}，图像高：{imgInfo[0]}，图像通道数：{imgInfo[2]}")

    fourcc = cv2.VideoWriter_fourcc(*"DIVX")  # 默认生成AVI格式
    if videoExtension.upper() == "mp4".upper():  # 如果想生成MP4格式
        fourcc = 0x7634706D
    videoName = videoName + "." + videoExtension.lower()
    if videoPath is None:
        videoPath = os.path.join(imgPath, videoName)
    else:
        if not os.path.exists(videoPath):
            os.makedirs(videoPath)
        videoPath = os.path.join(videoPath, videoName)

    videoWrite = cv2.VideoWriter(videoPath, fourcc, fps, frameSize)
    for imgFile in tqdm.tqdm(imgs):
        img = cv2.imread(imgFile)
        videoWrite.write(img)
    print(f"合并完成，请在{imgPath}下查看视频文件{videoName}!")
